require 'simplecov'

SimpleCov.start do
  add_filter "/spec/"
end

require File.expand_path '../../config.rb', __FILE__

Dir[File.expand_path 'spec/support/**/*.rb'].each { |f| require f }
