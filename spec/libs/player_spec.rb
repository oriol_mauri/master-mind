RSpec.describe Player do
  let(:player) { Player.new }

  describe '#victory' do
    context 'when player get the correct code' do
      before do
        player.results << { red: 4, white: 0 }
      end

      it 'should return true' do
        expect(player.victory).to be true
      end
    end

    context 'when did not get the correct code' do
      before do
        player.results = [ { red: 2, white: 0 }, { red: 1, white: 1 }, { red: 2, white: 1 }]
      end

      it 'should return false' do
        expect(player.victory).to be false
      end
    end
  end
end
