RSpec.describe Game do
  let(:gui) { Gui.new }
  let(:game) { Game.new(gui) }

  describe '#generate_pattern' do
    it 'should return array of colors' do
      expect(game.pattern.class).to eq Array
    end

    it 'should have length of level' do
      expect(game.pattern.length).to eq game.level
    end
  end

  describe '#valid_attempt?' do
    context 'correct attempt' do
      it 'should return true' do
        expect(game.send(:valid_attempt?, ['R', 'R', 'G', 'Y'])).to be true
      end
    end
    context 'short attempt' do
      it 'should return false' do
        expect(game.send(:valid_attempt?, ['R'])).to be false
      end
    end

    context 'long attempt' do
      it 'should return false' do
        expect(game.send(:valid_attempt?, ['R', 'R', 'R', 'R', 'R', 'R', 'R', 'R'])).to be false
      end
    end

    context 'wrong attempt' do
      it 'should return false' do
        expect(game.send(:valid_attempt?, ['R', 'R', 'R', 'COLOR_404'])).to be false
      end
    end
  end

  describe '#algorism' do
    before(:each) do
      allow_any_instance_of(Game).to receive(:generate_pattern).and_return(['R', 'Y', 'W', 'G'])
    end

    context 'correct convination' do
      it 'should return 4 red and 0 whites pegs' do
        expect(game.send(:algorism, ['R', 'Y', 'W', 'G'])).to eq({ red: 4, white: 0 })
      end
    end

    context '1 correct, 1 color' do
      it 'should return 1 red and 1 whites pegs' do
        expect(game.send(:algorism, ['R', 'P', 'P', 'Y'])).to eq({ red: 1, white: 1 })
      end
    end

    context '1 correct, 1 color of correct one' do
      it 'should return 1 red and 0 whites pegs' do
        expect(game.send(:algorism, ['R', 'R', 'P', 'P'])).to eq({ red: 1, white: 0 })
      end
    end

    context '4 correct colors not position' do
      it 'should return 4 red and 0 whites pegs' do
        expect(game.send(:algorism, ['Y', 'R', 'G', 'W'])).to eq({ red: 0, white: 4 })
      end
    end

    context '1 color correct, not position' do
      it 'should return 4 red and 0 whites pegs' do
        expect(game.send(:algorism, ['P', 'R', 'P', 'P'])).to eq({ red: 0, white: 1 })
      end
    end
  end
end
