## Master Mind

### Requirements

- Ruby2.5

- Use Docker

```
$ docker build -t master_mind .
$ docker exec -i -t <container-name> ruby main.rb
```
### Play

```
$ ruby main.rb
```

```
$ docker exec -i -t master-mind ruby main.rb
```

### Run tests

Install dependecies & run tests
```
$ bundle install
$ bundle exec rspec
```

if you are using docker to run the script, just log into the container to run the test command or run it usign docker run or docker exec. [More info here](https://hub.docker.com/_/ruby)

### Deploy to AWS Lambda

With this docker, or without it, would be easy to set and deploy this script to [AWS Lambda](https://aws.amazon.com/es/lambda/)

Following this simple steps: https://aws.amazon.com/getting-started/tutorials/run-serverless-code/
