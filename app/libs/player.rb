# Model Player
class Player
  attr_accessor :attempts, :results, :victory

  def initialize
    @attempts = []
    @results  = []
  end

  def victory
    @results.include?({ red: 4, white: 0 })
  end

  def profile
    case @results.length
    when 0..5
      'Diamond'
    when 5..7
      'Gold'
    when 7..9
      'Silver'
    else
      'Wood'
    end
  end
end
