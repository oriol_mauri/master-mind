# Game Controller
class Game
  COLORS  = ['R', 'G', 'B', 'P', 'Y', 'W']
  RESULTS = [:red, :white]
  DEFAULT_LEVEL = 4

  attr_accessor :pattern, :player, :gui, :level

  def initialize(gui, level = DEFAULT_LEVEL)
    @gui     = gui
    @level   = level
    @pattern = generate_pattern
    @player  = Player.new
  end

  def play
    @gui.header COLORS

    while !@player.victory
      @gui.print_message "Attempts: [#{@player.attempts.length}]"
      attempt = @gui.get_attempt
      check(attempt)
    end

    @gui.you_won @player.profile
  end

  private

  def generate_pattern
    @level.times.map { |el| COLORS.sample }
  end

  def check(attempt)
    return unless valid_attempt?(attempt)

    pegs = algorism(attempt)

    @player.attempts << attempt
    @player.results  << pegs

    @gui.print_message @player.attempts
    @gui.print_message pegs
  end

  def valid_attempt?(attempt)
    if attempt.length != @level
      @gui.print_message("! Lenght of the code must be #{level}.")

      return false
    end

    attempt.each do |a|
      if !COLORS.include?(a)
        @gui.print_message("! Invalid colors.")
        return false
      end
    end

    true
  end

  def algorism(attempt)
    pattern_copy = @pattern.clone
    pegs       = { red: 0, white: 0 }

    attempt.each_with_index do |color, i|
      if color == pattern_copy[i]
        pattern_copy[i] = nil
        pegs[:red] += 1
      end
    end
    attempt.each_with_index do |color, i|
      if pattern_copy.include? color
        pegs[:white] += 1
        pattern_copy.delete(color)
      end
    end

    pegs
  end
end
