# View
class Console
  def initialize
    p '======================'
    p '== MASTER MIND GAME =='
    p '======================'
  end

  def header(colors)
    p '> Lavel difficulty: 4'
    p "> COLORS: #{colors}"
    p '> Code ****'
    p '======================'
  end

  def get_attempt
    print '> Guess a code: '
    attempt = gets.chars[0..-2]

    attempt
  end

  def you_won(profile)
    p "Your Profile is #{profile}."
    p '¡¡ CONGRATULATIONS !! <='
    p '  \ (•◡•) / '
    p '¡¡ YOU WON !! <='
  end

  def print_message(message)
    p message
  end
end
